#pragma once

#include <fmod.hpp>
#include <fmod_common.h>
#include <string>
namespace FMODWrapper {

	class Channel {
	public:
		float GetVolume();
		void SetVolume(float);
		float GetPan();
		void SetPan(float);
		void SetLoop(bool);
		bool IsLooping();

		FMOD::Channel* channel;
	private:
		float pan;
		bool inLoop;
		void CalculatePan();
	};
	class FAudio {
	public:
		void Load(std::string fileName, bool isStreaming);
		Channel Play();
		void Pause(Channel* channel, bool active);
		void Stop(Channel* channel);
		void SetLoop(bool);
		bool IsLooping();
		bool GetStatusPlay();
	private:
		FMOD::Sound* audio;
		bool isPlaying;
	};

	class FModClass
	{
	public:
		static FModClass& GetInstance();
		void IsValid(FMOD_RESULT result);
		FMOD::System* system;
	private:
		void Init();
	};
}