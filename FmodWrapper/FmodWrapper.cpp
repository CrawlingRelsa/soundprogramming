#include "pch.h"


#include <iostream>
#include "FmodWrapper.h"


void FMODWrapper::FModClass::Init() {

	//Create a System object and initialize
	FMOD_RESULT result = FMOD::System_Create(&system);
	IsValid(result);

	//Init System
	result = system->init(32, FMOD_INIT_NORMAL, nullptr);
	IsValid(result);
}

FMODWrapper::FModClass& FMODWrapper::FModClass::GetInstance()
{
	static FModClass singleton;
	if (singleton.system == 0) {
		singleton.Init();
	}
	return singleton;
}

void FMODWrapper::FModClass::IsValid(FMOD_RESULT result) {
	if (result != FMOD_OK) {
		std::cout << "Errore n." << result;
		abort();
	}
}

float FMODWrapper::Channel::GetVolume()
{
	float volume = 0.f;
	FModClass manager = FModClass::GetInstance(); 
	manager.IsValid(this->channel->getVolume(&volume));
	return volume;
}

void FMODWrapper::Channel::SetVolume(float vol)
{
	FModClass manager = FModClass::GetInstance();
	manager.IsValid(this->channel->setVolume(vol));
}

float FMODWrapper::Channel::GetPan()
{
	return pan;
}

void FMODWrapper::Channel::SetPan(float val)
{
	FModClass manager = FModClass::GetInstance();
	manager.IsValid(this->channel->setPan(val));
	CalculatePan();
}

void FMODWrapper::Channel::SetLoop(bool loopValue)
{
	FModClass manager = FModClass::GetInstance();
	manager.IsValid(this->channel->setLoopCount(loopValue ? -1 : 0));
	inLoop = loopValue;
}

bool FMODWrapper::Channel::IsLooping()
{
	return inLoop;
}

void FMODWrapper::Channel::CalculatePan()
{
	int out, in;
	float mat[32][32];
	// Get the pan matrix
	this->channel->getMixMatrix(0, &out, &in, 32);
	this->channel->getMixMatrix(&mat[0][0], &out, &in, 32);

	// calculate a pan value based on front left/front right.
	float l = 1.0f - (2 * (mat[0][0] * mat[0][0]));
	float r = (2 * (mat[1][0] * mat[1][0]) - 1.0f);

	pan = (l + r) * 0.5f;

	pan < -1.0f ? -1.0f : pan > 1.0f ? 1.0f : pan;
}

void FMODWrapper::FAudio::Load(std::string fileName, bool isStreaming)
{
	FModClass manager = FModClass::GetInstance();
	FMOD_RESULT result = manager.system->createSound(fileName.c_str(), isStreaming? FMOD_CREATESTREAM : FMOD_DEFAULT, 0, &audio);
	manager.IsValid(result);
}

FMODWrapper::Channel FMODWrapper::FAudio::Play()
{
	FModClass manager = FModClass::GetInstance();
	FMOD::Channel* ch = 0;
	FMOD_RESULT result = manager.system->playSound(audio, 0, false, &ch);
	manager.IsValid(result);
	Channel c_return;
	c_return.channel = ch;
	isPlaying = true;
	return c_return;
}

void FMODWrapper::FAudio::Pause(Channel* channel, bool active)
{
	FModClass manager = FModClass::GetInstance();
	FMOD::Channel* ch = channel->channel;
	FMOD_RESULT result = ch->setPaused(active);
	manager.IsValid(result);
	isPlaying = active;
}

void FMODWrapper::FAudio::Stop(Channel* channel)
{
	FMOD::Channel* ch = channel->channel;
	FMOD_RESULT result = ch->stop();
}


void FMODWrapper::FAudio::SetLoop(bool loopValue)
{
	FModClass manager = FModClass::GetInstance();
	manager.IsValid(this->audio->setLoopCount( 5 /*loopValue ? -1 : 0*/));
}

bool FMODWrapper::FAudio::IsLooping()
{
	int a = 0;
	this->audio->getLoopCount(&a);
	return a >= 0;
}

bool FMODWrapper::FAudio::GetStatusPlay()
{
	return isPlaying;
}
