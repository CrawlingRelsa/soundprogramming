// FModExample.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>

#include <FmodWrapper.h>

#include <windows.h>

using namespace FMODWrapper;

std::string ExecPath();

int main()
{

	//find current dir
	std::string directory = ExecPath();
	//create jaguar audio
	std::string jaguarWavPath = directory + "\\drumloop.wav";
	FAudio drumloop;
	drumloop.Load(jaguarWavPath, false);


	Channel channelZero = drumloop.Play();
	channelZero.SetLoop(true);
	while (true) {

		char demo = 0;
		std::cout << "--------Scegli una demo--------" << std::endl;
		std::cout << "a - Play/Pause/Resume" << std::endl;
		std::cout << "b - Pan" << std::endl;
		std::cout << "c- Volume" << std::endl;
		demo = std::cin.get();
		if (demo == '\n') {
			demo = 0;
			continue;
		}
		if (demo == 'a') {
			while (true)
			{
				std::cin.clear();
				std::cout << "Press Enter to Pause/Resume. q for exit" << std::endl;
				char input = std::cin.get();
				if (input == 'q') break;
				drumloop.Pause(&channelZero, !drumloop.GetStatusPlay());

			}
		}
		else if (demo == 'b') {
			bool play = true;
			while (play)
			{
				std::cout << "Press a/s + Enter to move left/right the pan. q for exit" << std::endl;
				char a = std::cin.get();
				std::cin.clear();
				switch (a)
				{
				case 'a':
					channelZero.SetPan(-1);
					break;
				case 's':
					channelZero.SetPan(1);
					break;
				case 'q':
					play = false;
					break;
				default:
					break;
				}
			}
		}
		else if (demo == 'c') {
			bool play = true;
			while (play)
			{
				std::cin.clear();
				std::cout << "Press a/s + Enter to decrease/increase volume. q for exit" << std::endl;
				char a = std::cin.get();
				switch (a)
				{
				case 'a':
				{
					float vol = channelZero.GetVolume() - 0.1f;
					channelZero.SetVolume(vol <= 0 ? 0 : vol);
					break;
				}
				case 's':
					channelZero.SetVolume(channelZero.GetVolume() + 0.1f);
					break;
				case 'q':
					play = false;
					break;
				default:
					break;
				}
				//std::cout << "Volume at: " << channelZero.GetVolume() << std::endl;
			}
		}
		else {
			return 0;
		}
		std::cout << "\n\n-------------------------------" << std::endl;
	}
}


std::string ExecPath() {
	TCHAR buffer[MAX_PATH] = { 0 };
	GetModuleFileName(NULL, buffer, MAX_PATH);
	std::wstring::size_type pos = std::wstring(buffer).find_last_of(L"\\/");
	std::wstring bufferstring(buffer);
	bufferstring = bufferstring.substr(0, pos);
	std::string directory(bufferstring.begin(), bufferstring.end());
	return directory;
}
